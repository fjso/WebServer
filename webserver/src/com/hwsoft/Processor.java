package com.hwsoft;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processor  extends Thread{
       
	   private Socket socket;
	   private InputStream in;
	   private PrintStream out;
	   public final static String WEBROOT="c:\\web\\file";
	   
	   public Processor(Socket socket){
		   this.socket=socket;
		   try {
			in=socket.getInputStream();
			out=new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	   }
	   public void run(){
		   String filename=parse(in);
		   sendFile(filename);
			
		
		   
	   }
	   
	   public String parse(InputStream in){
		   BufferedReader br= new BufferedReader(new InputStreamReader(in));
		   String filename=null;
		   try {
			String httpMessage = br.readLine();
			String[] content=httpMessage.split(" ");
			if (content.length!=3){
				sendErrorMessage(400,"request error!");
				return null;
			}
			System.out.println("code:"+content[0]+","+"filename:"+content[1]+",version:"+content[2]);
			filename=content[1];
		   
		   } catch (IOException e) {
				e.printStackTrace();
			}
		   return filename;
	   }
	   
	   public void sendErrorMessage(int errorCode,String errorMessage){
		   out.println("http/1.0 404"+errorCode+","+errorMessage);
		   out.println("content-type:text/html");
		   out.println();
		   out.println("<html>");
		   out.println("<title>Error Message");
		   out.println("/<title>");
		   out.println("<body>");
		   out.println("<H1>errorCode:"+errorCode+",errorMessage"+errorMessage+"/<H1>");
		   out.println("/<body>");
		   out.println("/<html>");
		   out.flush();
		   out.close();
		   try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	   }
	   
	   public void sendFile(String filename){
		   File file = new File(this.WEBROOT+filename);
		   if (!file.exists()){
			   sendErrorMessage(404,"file not fond!");
			   return;
		   }
		   try {
				InputStream in = new FileInputStream(file);
				byte[] b = new byte[(int)file.length()];
			    in.read(b);
			    out.println("http/1.0 200 findfile");
			    out.println("content-length:"+b.length);
			    out.println();
			    out.write(b);
			    out.flush();
			    out.close();
			    in.close();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
		    }catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
		    }
	        
}
}
